<?php
    include './vendor/autoload.php';

    use MyNamespace\MyIterator;
    use Modifier\{
        CubeModifier,
        SinhModifier,
        LogModifier
    };

    $array = [
        2,
        7,
        11,
        1,
        9,
        56,
        12,
    ];

    $iterator = new MyIterator($array);
    $iterator->setModifier(new CubeModifier());

    $iterator2 = new MyIterator($array);
    $iterator2->setModifier(new SinhModifier());

    $iterator2 = new MyIterator($array);
    $iterator2->setModifier(new SinhModifier());

    $iterator3 = new MyIterator($array);
    $iterator3->setModifier(new LogModifier());

    foreach ($iterator3 as $k => $v) {
        print_r("Element ".$k.": ".$v."\n");
    }
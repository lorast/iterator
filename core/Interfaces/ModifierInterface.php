<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/11/17
 * Time: 2:50 PM
 */

namespace MyInterface;


interface ModifierInterface
{
    public function handleNumber (int $number);
}
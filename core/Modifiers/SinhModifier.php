<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/11/17
 * Time: 4:04 PM
 */

namespace Modifier;

use MyInterface\ModifierInterface;

class SinhModifier implements ModifierInterface
{
    public function handleNumber(int $number)
    {
        $result = sinh($number);

        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/11/17
 * Time: 2:53 PM
 */

namespace Modifier;

use MyInterface\ModifierInterface;

class CubeModifier implements ModifierInterface
{
    /**
     * @param int $number
     * @return float|int
     */
    public function handleNumber(int $number)
    {
        $result = pow($number, 3);

        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/11/17
 * Time: 4:14 PM
 */

namespace Modifier;


use MyInterface\ModifierInterface;

class LogModifier implements ModifierInterface
{
    public function handleNumber(int $number)
    {
        $result = log($number);

        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/11/17
 * Time: 2:41 PM
 */

namespace MyNamespace;

use MyInterface\ModifierInterface;

class MyIterator implements \Iterator
{
    private $position = 0;
    private $modifier;
    private $num_array;
    private $mod_array;

    public function __construct (array $num_arr)
    {
        $this->position = 0;
        $this->num_array = $num_arr;
    }

    public function setModifier (ModifierInterface $modifier)
    {
        $this->modifier = $modifier;
    }

    public function next()
    {
        $this->position += 1;
    }

    public function current()
    {
        if (isset($this->modifier)) {
            $this->mod_array[$this->position] = $this->modifier->handleNumber($this->num_array[$this->position]);

            return $this->mod_array[$this->position];
        } else {
            return $this->num_array[$this->position];
        }
    }

    public function key()
    {
        return $this->position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return isset($this->num_array[$this->position]);
    }
}